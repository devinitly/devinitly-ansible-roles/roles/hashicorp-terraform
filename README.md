Terraform
=========

This Ansible role installs Terraform on linux amd64 based systems.

The installation path for the terraform binary is /usr/local/bin/terraform.

Requirements
------------

- wget (installed)

Role Variables
--------------

The Terraform role allows varibles to be set for the linux system user and group permisions.

By default the role will supply Terraform version 0.12.2 if no versions is defined.

Default:
terraform_version: 0.12.2

vars:
- owner: # desired system owner
- group: # desired system group

Example Playbook
----------------
```yml
---
- hosts: servers
  vars:
   - terraform_version: 0.11.7
   - owner: root
   - group: root
     roles:
         - terraform 
```

License
-------

BSD
